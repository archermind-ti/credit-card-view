/*
 * Copyright (C) 2015 Vinay Gaba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.vinaygaba.creditcardview;

import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;
import ohos.hiviewdfx.HiLogLabel;

import java.io.*;
import java.util.regex.Pattern;

import static com.vinaygaba.creditcardview.CardNumberFormat.*;
import static com.vinaygaba.creditcardview.CardType.*;

public class CreditCardView extends DependentLayout {
    public static final HiLogLabel TAG = new HiLogLabel(3, 0xD001100, "CreditCardView");
    private static final int CARD_FRONT = 0;
    private static final int CARD_BACK = 1;
    private static final boolean DEBUG = false;
    private String mCardNumber;
    private String mCardName;
    private String mExpiryDate;
    private String mCvv;
    private String mFontPath;
    private int mCardNumberTextColor = Color.WHITE.getValue();
    private int mCardNumberFormat = ALL_DIGITS;
    private int mCardNameTextColor = Color.WHITE.getValue();
    private int mExpiryDateTextColor = Color.WHITE.getValue();
    private int mCvvTextColor = Color.BLACK.getValue();
    private int mValidTillTextColor = Color.WHITE.getValue();
    private int mType = VISA;
    private Element mBrandLogo;
    private int cardSide = CARD_FRONT;
    private boolean mPutChip;
    private boolean mIsEditable;
    private boolean mIsCardNumberEditable;
    private boolean mIsCardNameEditable;
    private boolean mIsExpiryDateEditable;
    private boolean mIsCvvEditable;
    private boolean mIsFlippable;
    private int mHintTextColor = Color.WHITE.getValue();
    private int mCvvHintColor = Color.WHITE.getValue();
    private Element mCardFrontBackground;
    private Element mCardBackBackground;
    private Image mFlipBtn;
    private TextField mCardNumberView;
    private TextField mCardNameView;
    private TextField mExpiryDateView;
    private TextField mCvvView;
    private Image mCardTypeView;
    private Image mBrandLogoView;
    private Image mChipView;
    private Text mValidTill;
    private Image mStripe;
    private Text mAuthorizedSig;
    private Image mSignature;

    public CreditCardView(Context context) {
        this(context, null);
    }

    public CreditCardView(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public CreditCardView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
        loadAttributes(attrSet);
        initDefaults();
        addListeners();
    }

    private void init() {
        LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_layout_credit_card_view, this, true);
        mCardNumberView = (TextField) findComponentById(ResourceTable.Id_card_number);
        mCardNameView = (TextField) findComponentById(ResourceTable.Id_card_name);
        mCardTypeView = (Image) findComponentById(ResourceTable.Id_card_logo);
        mBrandLogoView = (Image) findComponentById(ResourceTable.Id_brand_logo);
        mChipView = (Image) findComponentById(ResourceTable.Id_chip);
        mValidTill = (Text) findComponentById(ResourceTable.Id_valid_till);
        mExpiryDateView = (TextField) findComponentById(ResourceTable.Id_expiry_date);
        mFlipBtn = (Image) findComponentById(ResourceTable.Id_flip_btn);
        mCvvView = (TextField) findComponentById(ResourceTable.Id_cvv_et);
        mStripe = (Image) findComponentById(ResourceTable.Id_stripe);
        mAuthorizedSig = (Text) findComponentById(ResourceTable.Id_authorized_sig_tv);
        mSignature = (Image) findComponentById(ResourceTable.Id_signature);
    }

    private void loadAttributes(AttrSet attrSet) {
        mCardNumber = AttrUtils.getStringFromAttr(attrSet, "cardNumber", "");
        mCardName = AttrUtils.getStringFromAttr(attrSet, "cardName", "");
        mExpiryDate = AttrUtils.getStringFromAttr(attrSet, "expiryDate", "");
        mCardNumberTextColor = AttrUtils.getColorFromAttr(attrSet, "cardNumberTextColor", Color.WHITE.getValue());
        mCardNumberFormat = AttrUtils.getIntFromAttr(attrSet, "cardNumberFormat", 0);
        mCardNameTextColor = AttrUtils.getColorFromAttr(attrSet, "cardNumberTextColor", Color.WHITE.getValue());
        mExpiryDateTextColor = AttrUtils.getColorFromAttr(attrSet, "expiryDateTextColor", Color.WHITE.getValue());
        mCvvTextColor = AttrUtils.getColorFromAttr(attrSet, "cvvTextColor", Color.BLACK.getValue());
        mValidTillTextColor = AttrUtils.getColorFromAttr(attrSet, "validTillTextColor", Color.WHITE.getValue());
        mType = AttrUtils.getIntFromAttr(attrSet, "type", VISA);
        mBrandLogo = AttrUtils.getElementFromAttr(attrSet, "brandLogo", null);
        mPutChip = AttrUtils.getBooleanFromAttr(attrSet, "putChip", false);
        mIsEditable = AttrUtils.getBooleanFromAttr(attrSet, "isEditable", false);
        mIsCardNameEditable = AttrUtils.getBooleanFromAttr(attrSet, "isCardNameEditable", mIsEditable);
        mIsCardNumberEditable = AttrUtils.getBooleanFromAttr(attrSet, "isCardNumberEditable", mIsEditable);
        mIsExpiryDateEditable = AttrUtils.getBooleanFromAttr(attrSet, "isExpiryDateEditable", mIsEditable);
        mIsCvvEditable = AttrUtils.getBooleanFromAttr(attrSet, "isCvvEditable", mIsEditable);
        mHintTextColor = AttrUtils.getColorFromAttr(attrSet, "hintTextColor", Color.WHITE.getValue());
        mIsFlippable = AttrUtils.getBooleanFromAttr(attrSet, "isFlippable", false);
        mCvv = AttrUtils.getStringFromAttr(attrSet, "cvv", "");
        mCardFrontBackground = AttrUtils.getElementFromAttr(attrSet, "cardFrontBackground", ResUtil.getAnyElementByResId(mContext, ResourceTable.Media_cardbackground_sky));
        mCardBackBackground = AttrUtils.getElementFromAttr(attrSet, "cardBackBackground", ResUtil.getAnyElementByResId(mContext, ResourceTable.Graphic_cardbackground_canvas));
        mFontPath = AttrUtils.getStringFromAttr(attrSet, "fontPath", "");
    }

    private void initDefaults() {
        try {
            setBackground(mCardFrontBackground);
        } catch (Exception e) {

        }

        if (isEmpty(mFontPath)) {
            mFontPath = "halter.ttf";
        }

        if (!mIsEditable) {
            mCardNumberView.setEnabled(false);
            mCardNameView.setEnabled(false);
            mExpiryDateView.setEnabled(false);
            mCvvView.setEnabled(false);
        } else {
            mCardNumberView.setHint("ENTER CARD NUMBER");
            mCardNumberView.setHintColor(new Color(mHintTextColor));

            mCardNameView.setHint("ENTER CARD NAME");
            mCardNameView.setHintColor(new Color(mHintTextColor));

            mExpiryDateView.setHint("MM/YY");
            mExpiryDateView.setHintColor(new Color(mHintTextColor));

            mCvvView.setHint("cvv");
            mCvvView.setHintColor(new Color(mCvvTextColor));
        }

        if (mIsCardNameEditable != mIsEditable) {
            if (mIsCardNameEditable) {
                mCardNameView.setHint("ENTER CARD NAME");
                mCardNameView.setHintColor(new Color(mHintTextColor));
            } else {
                mCardNameView.setHint("");
            }

            mCardNameView.setEnabled(mIsCardNameEditable);
        }

        if (mIsCardNumberEditable != mIsEditable) {
            if (mIsCardNumberEditable) {
                mCardNumberView.setHint("ENTER CARD NUMBER");
                mCardNumberView.setHintColor(new Color(mHintTextColor));
            } else {
                mCardNumberView.setHint("");
            }
            mCardNumberView.setEnabled(mIsCardNumberEditable);
        }

        if (mIsExpiryDateEditable != mIsEditable) {
            if (mIsExpiryDateEditable) {
                mExpiryDateView.setHint("MM/YY");
                mExpiryDateView.setHintColor(new Color(mHintTextColor));
            } else {
                mExpiryDateView.setHint("");
            }
            mExpiryDateView.setEnabled(mIsExpiryDateEditable);
        }

        if (!isEmpty(mCardNumber)) {
            mCardNumberView.setText(getFormattedCardNumber(addSpaceToCardNumber()));
        }

        mCardNumberView.setTextColor(new Color(mCardNumberTextColor));

        if (!isInEditMode()) {
            setFontType(mCardNumberView, mFontPath);
        }

        if (!isEmpty(mCardName)) {
            mCardNameView.setText(mCardName.toUpperCase());
        }

        mCardNameView.setTextColor(new Color(mCardNumberTextColor));

        if (!isInEditMode()) {
            setFontType(mCardNameView, mFontPath);
        }

        mCardTypeView.setPixelMap(getLogo());

        if (mBrandLogo != null) {
            mBrandLogoView.setImageElement(mBrandLogo);
        }

        if (mPutChip) {
            mChipView.setVisibility(Component.VISIBLE);
        }

        if (!isEmpty(mExpiryDate)) {
            mExpiryDateView.setText(mExpiryDate);
        }

        mExpiryDateView.setTextColor(new Color(mExpiryDateTextColor));

        if (!isInEditMode()) {
            setFontType(mExpiryDateView, mFontPath);
        }

        mValidTill.setTextColor(new Color(mValidTillTextColor));

        if (!isEmpty(mCvv)) {
            mCvvView.setText(mCvv);
        }

        mCvvView.setTextColor(new Color(mCvvTextColor));

        if (!isInEditMode()) {
            setFontType(mCvvView, mFontPath);
        }

        if (mIsCvvEditable != mIsEditable) {

            if (mIsCvvEditable) {
                mCvvView.setHint("cvv");
                mCvvView.setHintColor(new Color(mCvvHintColor));
            } else {
                mCvvView.setHint("");
            }

            mCvvView.setEnabled(mIsCvvEditable);

        }

        if (mIsFlippable) {
            mFlipBtn.setVisibility(Component.VISIBLE);
        }
        mFlipBtn.setEnabled(mIsFlippable);
    }

    private void addListeners() {

        mCardNumberView.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int i, int i1, int i2) {
                mType = AUTO;
                mCardNumber = s.replaceAll("\\s+", "");
            }
        });
        mCardNumberView.setFocusChangedListener(new FocusChangedListener() {
            @Override
            public void onFocusChange(Component component, boolean hasFocus) {
                if (!hasFocus) {
                    if (!isEmpty(mCardNumber) && mCardNumber.length() > 12) {
                        // If card type is "auto",find the appropriate logo
                        if (mType == AUTO) {
                            mCardTypeView.setPixelMap(getLogo());
                        }

                        // If the length of card is >12, add space every 4 characters and format it
                        // in the appropriate format
                        mCardNumberView.setText(getFormattedCardNumber(addSpaceToCardNumber()));
                    }
                }
            }
        });

        mCardNameView.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int i, int i1, int i2) {
                mCardName = s.toUpperCase();
            }
        });

        mExpiryDateView.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int i, int i1, int i2) {
                mExpiryDate = s;

            }
        });

        mFlipBtn.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                flip();
            }
        });
    }

    public boolean isFlippable() {
        return mIsFlippable;
    }

    public void setIsFlippable(boolean flippable) {
        mIsFlippable = flippable;
        mFlipBtn.setVisibility(mIsFlippable ? Component.VISIBLE : Component.INVISIBLE);
        mFlipBtn.setEnabled(mIsFlippable);
    }

    private void showFrontView() {
        mCardNumberView.setVisibility(Component.VISIBLE);
        mCardNameView.setVisibility(Component.VISIBLE);
        mCardTypeView.setVisibility(Component.VISIBLE);
        mBrandLogoView.setVisibility(Component.VISIBLE);
        if (mPutChip) {
            mChipView.setVisibility(Component.VISIBLE);
        }
        mValidTill.setVisibility(Component.VISIBLE);
        mExpiryDateView.setVisibility(Component.VISIBLE);
    }

    private void hideFrontView() {
        mCardNumberView.setVisibility(Component.HIDE);
        mCardNameView.setVisibility(Component.HIDE);
        mCardTypeView.setVisibility(Component.HIDE);
        mBrandLogoView.setVisibility(Component.HIDE);
        mChipView.setVisibility(Component.HIDE);
        mValidTill.setVisibility(Component.HIDE);
        mExpiryDateView.setVisibility(Component.HIDE);
    }

    private void showBackView() {
        mStripe.setVisibility(Component.VISIBLE);
        mAuthorizedSig.setVisibility(Component.VISIBLE);
        mSignature.setVisibility(Component.VISIBLE);
        mCvvView.setVisibility(Component.VISIBLE);
    }

    private void hideBackView() {
        mStripe.setVisibility(Component.HIDE);
        mAuthorizedSig.setVisibility(Component.HIDE);
        mSignature.setVisibility(Component.HIDE);
        mCvvView.setVisibility(Component.HIDE);
    }

    private void redrawViews() {
        invalidate();
        postLayout();
    }

    public String getCardNumber() {
        return mCardNumber;
    }

    public void setCardNumber(String cardNumber) {
        if (cardNumber == null) {
            throw new NullPointerException("Card Number cannot be null.");
        }
        this.mCardNumber = cardNumber.replaceAll("\\s+", "");
        this.mCardNumberView.setText(addSpaceToCardNumber());
        redrawViews();
    }

    public String getCardName() {
        return mCardName;
    }

    public void setCardName(String cardName) {
        if (cardName == null) {
            throw new NullPointerException("Card Name cannot be null.");
        }
        this.mCardName = cardName.toUpperCase();
        this.mCardNameView.setText(mCardName);
        redrawViews();
    }

    public int getCardNumberTextColor() {
        return mCardNumberTextColor;
    }

    public void setCardNumberTextColor(int cardNumberTextColor) {
        this.mCardNumberTextColor = cardNumberTextColor;
        this.mCardNumberView.setTextColor(new Color(mCardNumberTextColor));
        redrawViews();
    }

    public void setCardNumberFormat(int cardNumberFormat) {
        if (cardNumberFormat < 0 | cardNumberFormat > 3) {
            throw new UnsupportedOperationException("CardNumberFormat: " + cardNumberFormat + "  " +
                    "is not supported. Use `CardNumberFormat.*` or `CardType.ALL_DIGITS` if " +
                    "unknown");
        }
        this.mCardNumberFormat = cardNumberFormat;
        this.mCardNumberView.setText(getFormattedCardNumber(mCardNumber));
        redrawViews();
    }

    public int getCardNameTextColor() {
        return mCardNameTextColor;
    }

    public void setCardNameTextColor(int cardNameTextColor) {
        this.mCardNameTextColor = cardNameTextColor;
        this.mCardNameView.setTextColor(new Color(mCardNameTextColor));
        redrawViews();
    }

    public String getExpiryDate() {
        return mExpiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.mExpiryDate = expiryDate;
        this.mExpiryDateView.setText(mExpiryDate);
        redrawViews();
    }

    public int getExpiryDateTextColor() {
        return mExpiryDateTextColor;
    }

    public void setExpiryDateTextColor(int expiryDateTextColor) {
        this.mExpiryDateTextColor = expiryDateTextColor;
        this.mExpiryDateView.setTextColor(new Color(mExpiryDateTextColor));
        redrawViews();
    }

    public int getValidTillTextColor() {
        return mValidTillTextColor;
    }

    public void setValidTillTextColor(int validTillTextColor) {
        this.mValidTillTextColor = validTillTextColor;
        this.mValidTill.setTextColor(new Color(mValidTillTextColor));
        redrawViews();
    }

    public int getType() {
        return mType;
    }

    public void setType(int type) {
        if (type < 0 | type > 4) {
            throw new UnsupportedOperationException("CardType: " + type + "  is not supported. " +
                    "Use `CardType.*` or `CardType.AUTO` if unknown");
        }
        this.mType = type;
        this.mCardTypeView.setPixelMap(getLogo());
        redrawViews();
    }

    public boolean getIsEditable() {
        return mIsEditable;
    }

    public void setIsEditable(boolean isEditable) {
        this.mIsEditable = isEditable;
        redrawViews();
    }

    public boolean getIsCardNameEditable() {
        return mIsCardNameEditable;
    }

    public void setIsCardNameEditable(boolean isCardNameEditable) {
        this.mIsCardNameEditable = isCardNameEditable;
        redrawViews();
    }

    public boolean getIsCardNumberEditable() {
        return mIsCardNumberEditable;
    }

    public void setIsCardNumberEditable(boolean isCardNumberEditable) {
        this.mIsCardNumberEditable = isCardNumberEditable;
        redrawViews();
    }

    public boolean getIsExpiryDateEditable() {
        return mIsExpiryDateEditable;
    }

    public void setIsExpiryDateEditable(boolean isExpiryDateEditable) {
        this.mIsExpiryDateEditable = isExpiryDateEditable;
        redrawViews();
    }

    public int getHintTextColor() {
        return mHintTextColor;
    }

    public void setHintTextColor(int hintTextColor) {
        this.mHintTextColor = hintTextColor;
        this.mCardNameView.setHintColor(new Color(mHintTextColor));
        this.mCardNumberView.setHintColor(new Color(mHintTextColor));
        this.mExpiryDateView.setHintColor(new Color(mHintTextColor));

        redrawViews();
    }

    public Element getBrandLogo() {
        return mBrandLogo;
    }

    public void setBrandLogo(Element brandLogo) {
        this.mBrandLogo = brandLogo;
        this.mBrandLogoView.setImageElement(mBrandLogo);
        redrawViews();
    }

    public void putChip(boolean flag) {
        this.mPutChip = flag;
        this.mChipView.setVisibility(mPutChip ? Component.VISIBLE : Component.HIDE);
        redrawViews();
    }

    public boolean getIsCvvEditable() {
        return mIsCvvEditable;
    }

    public void setIsCvvEditable(boolean editable) {
        this.mIsCvvEditable = editable;
        redrawViews();
    }

    public Element getCardBackBackground() {
        return mCardBackBackground;
    }

    public void setCardBackBackground(Element cardBackBackground) {
        this.mCardBackBackground = cardBackBackground;
        setBackground(mCardBackBackground);
        redrawViews();
    }

    public Element getCardFrontBackground() {
        return mCardFrontBackground;
    }

    public void setCardFrontBackground(Element mCardFrontBackground) {
        this.mCardFrontBackground = mCardFrontBackground;
        setBackground(mCardFrontBackground);
        redrawViews();
    }

    public String getFontPath() {
        return mFontPath;
    }

    public void setFontPath(String mFontPath) {
        this.mFontPath = mFontPath;
        if (!isInEditMode()) {
            setFontType(mCardNumberView, mFontPath);
            setFontType(mCardNameView, mFontPath);
            setFontType(mExpiryDateView, mFontPath);
            setFontType(mCvvView, mFontPath);
        }
        redrawViews();
    }

    public void flip() {
        if (mIsFlippable) {
            if (cardSide == CARD_FRONT) {
                rotateInToBack();
            } else if (cardSide == CARD_BACK) {
                rotateInToFront();
            }

        }
    }

    private int getLogo() {
        switch (mType) {
            case VISA:
                return ResourceTable.Media_visa;

            case MASTERCARD:
                return ResourceTable.Media_mastercard;

            case AMERICAN_EXPRESS:
                return ResourceTable.Media_amex;

            case DISCOVER:
                return ResourceTable.Media_discover;

            case AUTO:
                return findCardType();

            default:
                throw new UnsupportedOperationException("CardType: " + mType + "  is not supported" +
                        ". Use `CardType.*` or `CardType.AUTO` if unknown");
        }

    }

    private int findCardType() {
        this.mType = VISA;
        if (!isEmpty(mCardNumber)) {
            final String cardNumber = mCardNumber.replaceAll("\\s+", "");

            if (Pattern.compile(PATTERN_MASTER_CARD).matcher(cardNumber).matches()) {
                this.mType = MASTERCARD;
            } else if (Pattern.compile(PATTERN_AMERICAN_EXPRESS).matcher(cardNumber).matches()) {
                this.mType = AMERICAN_EXPRESS;
            } else if (Pattern.compile(PATTERN_DISCOVER).matcher(cardNumber).matches()) {
                this.mType = DISCOVER;
            }
        }
        return getLogo();
    }

    private String addSpaceToCardNumber() {

        final int splitBy = 4;
        final int length = mCardNumber.length();

        if (length % splitBy != 0 || length <= splitBy) {
            return mCardNumber;
        } else {
            final StringBuilder result = new StringBuilder();
            result.append(mCardNumber.substring(0, splitBy));
            for (int i = splitBy; i < length; i++) {
                if (i % splitBy == 0) {
                    result.append(" ");
                }
                result.append(mCardNumber.charAt(i));
            }
            return result.toString();
        }
    }

    private String getFormattedCardNumber(String cardNumber) {
        switch (getCardNumberFormat()) {
            case MASKED_ALL_BUT_LAST_FOUR:
                cardNumber = "**** **** **** " + cardNumber.substring(cardNumber.length() - 4);
                break;
            case ONLY_LAST_FOUR:
                cardNumber = cardNumber.substring(cardNumber.length() - 4);
                break;
            case MASKED_ALL:
                cardNumber = "**** **** **** ****";
                break;
            case ALL_DIGITS:
                //do nothing.
                break;
            default:
                throw new UnsupportedOperationException("CreditCardFormat: " + mCardNumberFormat +
                        " is not supported. Use `CreditCardFormat.*`");
        }
        return cardNumber;
    }

    public int getCardNumberFormat() {
        return mCardNumberFormat;
    }

    private boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    private boolean isInEditMode() {
        return false;
    }

    private void rotateInToBack() {
        rotateOutToBack();
    }

    private void rotateInToFront() {
        rotateOutToFront();
    }

    private void rotateOutToBack() {
        hideFrontView();
        showBackView();
        setBackground(mCardBackBackground);
        cardSide = CARD_BACK;
    }

    private void rotateOutToFront() {
        showFrontView();
        hideBackView();
        setBackground(mCardFrontBackground);
        cardSide = CARD_FRONT;
    }

    private void setFontType(Text text, String fontTypeName) {
        ResourceManager resManager = getContext().getResourceManager();
        RawFileEntry rawFileEntry = resManager.getRawFileEntry("resources/rawfile/font/" + fontTypeName);
        Resource resource = null;
        try {
            resource = rawFileEntry.openRawFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuffer fileName = new StringBuffer("halter.ttf");
        File file = new File(mContext.getCacheDir(), fileName.toString());
        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(file);
            int index;
            byte[] bytes = new byte[1024];
            while ((index = resource.read(bytes)) != -1) {
                outputStream.write(bytes, 0, index);
                outputStream.flush();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                resource.close();
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Font.Builder builder = new Font.Builder(file);
        text.setFont(builder.build());
    }
}
