# CreditCardView

### 简介
提供一个信用卡样式组件布局


### 功能
信用卡样式组件布局

### 演示
<img src="./demo/demo1.png" width="30%" height="30%">
<img src="./demo/demo2.png" width="30%" height="30%">


### 集成
- 方式一：添加远程依赖

```
dependencies {
	        compile 'com.gitee.archermind-ti:creditcardview_ohos:1.0.0'
	}

```

- 方式二：通过har形式导入

1. 下载har包[creditcardview_ohos-1.0.0.har](https://gitee.com/archermind-ti/credit-card-view/releases/V1.0.0-release)。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下jar包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

### 使用说明

- 在xml布局中使用控件

```
 <com.vinaygaba.creditcardview.CreditCardView
            ohos:height="225vp"
            ohos:width="match_parent"
            ohos:bottom_margin="16vp"
            ohos:top_margin="16vp"
            hap:brandLogo="$media:brandlogo"
            hap:cardFrontBackground="$media:cardbackground_world"
            hap:cardName="John Doe"
            hap:cardNameTextColor="#cccccc"
            hap:cardNumberFormat="all_digits"
            hap:cardNumberTextColor="#cccccc"
            hap:expiryDate="11/15"
            hap:expiryDateTextColor="#cccccc"
            hap:hintTextColor="#ffffff"
            hap:isCardNumberEditable="true"
            hap:isEditable="false"
            hap:isFlippable="true"
            hap:putChip="true"
            hap:type="1"
            hap:validTillTextColor="#cccccc"/>
```

### 编译说明
1.将项目通过git clone 至本地 2.使用DevEco Studio 打开该项目，然后等待Gradle 构建完成 3.点击Run运行即可（真机运行可能需要配置签名）

### 版本迭代
- v1.0.0

### 版权和许可信息
Copyright 2015 Vinay Gaba

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.